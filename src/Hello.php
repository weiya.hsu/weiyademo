<?php

namespace weiya;

class Hello
{
    private $say = 'hello world!';

    public function run($var = null)
    {
        return "run {$var}";
    }

    public function test()
    {
        return $this->say;
    }
}
